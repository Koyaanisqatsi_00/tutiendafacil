<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Multimedia;
use Image;
use Storage;

class ReducirImagenes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imagenes:reducir';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reduce el tamaño de las imagenes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $storage            = Storage::disk('public')->allFiles();
        $count              = 0;

        $this->info("Cantidad de imagenes en public es: ".count($storage));
        $this->info('--');
        $this->newLine(2);
        
        $bar = $this->output->createProgressBar(count($storage));

        $bar->start();

        foreach($storage as $imagen)
        {
            $extencion = explode('.', $imagen)[1];
            
            if(($extencion == "jpeg") or ($extencion == "jpg") or ($extencion == "png") or($extencion == "gif"))
            {
                $nombre = public_path('storage/'.$imagen);
                
                if(file_exists(public_path('storage/'.$imagen)))
                {
                    $img    = Image::make($nombre);

                    if($img->filesize() > "1000000")
                    {
                        $img->resize(500, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });

                        $img->save($nombre, 90);
                        $count ++;
                    }
                }

                $bar->advance();
            }
        }

        $bar->finish();

        $this->newLine(2);
        $this->info('Se modifico el tamaño de '.$count.' imagenes');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Producto;

class AgregarNumerosImagenes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imagenes:numeros';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agrega numero a cada imagen de un producto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $productos  = Producto::all();
        
        $bar = $this->output->createProgressBar(count($productos));

        foreach ($productos as $producto) 
        {
            $principal  = $producto->multimedia->where('tipo', 'principal')->first();

            if($principal != null)
            {
                $principal->numero = 0;
                $principal->save();

                $numero     = 1;

                foreach($producto->multimedia->where('tipo', '!=', 'principal') as $multimedia)
                {
                    $multimedia->numero = $numero;
                    $multimedia->save();
                    $numero ++;
                }
            }else
            {
                $numero     = 0;

                foreach($producto->multimedia as $multimedia)
                {
                    $multimedia->numero = $numero;
                    $multimedia->save();
                    $numero ++;
                }
            }

            $bar->advance();
        }

        $bar->finish();

        $this->newLine();
        $this->info('Se agregaron los numeros a las imagenes');
    }
}

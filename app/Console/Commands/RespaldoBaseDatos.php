<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PhpZip\ZipFile;
use DB;
use Storage;

class RespaldoBaseDatos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respaldo base de datos y carpeta storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $db_name        = 'database_'.date('d_m_Y_H_i').'.sql';
        $storage_name   = 'storage_'.date('d_m_Y_H_i').'.zip';
        $empresas_name  = 'empresas_'.date('d_m_Y_H_i').'.zip';
        
        //Respaldo de la base de datos
        
        $command = "mysqldump --user=" . env('DB_USERNAME') ." --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . " > " . storage_path("backups/" . $db_name);

        exec($command);

        //Respaldo de la carpeta storage
        $zip_storage    = new ZipFile();
        
        $zip_storage
            ->addDirRecursive(storage_path('app/public'))
            ->saveAsFile(storage_path("backups/".$storage_name))
            ->close(); 

        $zip_empresas    = new ZipFile();
        
        $zip_empresas
            ->addDirRecursive(resource_path('views/empresas'))
            ->saveAsFile(storage_path("backups/".$empresas_name))
            ->close(); 

        Storage::disk('ftp')->put($db_name, fopen(storage_path("backups/".$db_name), 'r+'));
        Storage::disk('ftp')->put($empresas_name, fopen(storage_path("backups/".$empresas_name), 'r+'));
        Storage::disk('ftp')->put($storage_name, fopen(storage_path("backups/".$storage_name), 'r+'));

        $data = [];
        email('email.respaldo', "Respaldo de base de datos", $data, 'admin@tutiendafacil.uy');

        unlink(storage_path("backups/".$db_name));
        unlink(storage_path("backups/".$storage_name));
        unlink(storage_path("backups/".$empresas_name));
        
        $this->info("Se realiza el respaldo de la base de datos");
    }
}

<div class="row">
	@if($multimedia->count() == 0)
		<div class="col-12 text-center text-secondary">
			<i class="fa fa-3x fa-images"></i>
			<p>El producto no tiene imagenes</p>
		</div>
	@else
		@foreach($multimedia->sortBy('numero') as $imagen)
			<div class="col-6 col-md-3 col-lg-2 contenedor_imagen_producto" id="imagen_{{$imagen->numero}}" attr-url="{{asset($imagen->url)}}">
				
				<img src="{{asset($imagen->url)}}" alt="" width="100%" class="imagen_producto">

				@if(!$loop->first)
					<a class="btn_mover_imagen btn_mover_imagen_izquierda" attr-id="{{$imagen->id}}" attr-producto="{{$imagen->producto->id}}" attr-numero="{{$imagen->numero}}">
						<i class="fa fa-angle-left"></i>
					</a>
				@endif
				@if(!$loop->last)
					<a class="btn_mover_imagen btn_mover_imagen_derecha" attr-id="{{$imagen->id}}" attr-producto="{{$imagen->producto->id}}" attr-numero="{{$imagen->numero}}">
						<i class="fa fa-angle-right"></i>
					</a>	
				@endif

				<a class="boton_eliminar_imagen" data-toggle="modal" data-target="#deleteModal_{{$imagen->id}}">
					<i class="fa fa-trash"></i>
				</a>

				<!-- Modal -->
				<div class="modal fade deleteModal" id="deleteModal_{{$imagen->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
					<div class="modal-dialog modal-dialog-centered" role="document">
				  		<div class="modal-content">
							<div class="modal-header bg-gradient-danger">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times</span>
								</button>
				    		</div>
				    		<div class="modal-body text-center">
				    			<p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
				      			<h4>¿Desea eliminar el item?</h4>
				      			<br>
				            <hr>
				      			<div class="row">
				      				<div class="col">
			    						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
			    							NO
			    						</button>
				      				</div>
				      				<div class="col">
				      					<a class="boton_eliminar btn btn-danger btn-block text-white" attr-id="{{$imagen->id}}" attr-producto="{{$imagen->producto->id}}" >
											SI
										</a>					
				      				</div>
				      			</div>
				    		</div>
				  		</div>
					</div>
				</div>
			</div>	
		@endforeach	
	@endif
</div>

<script>
	$(document).ready(function()
	{
		$('.boton_eliminar').click(function()
		{ 
			var id 			= $(this).attr('attr-id');
			var producto 	= $(this).attr('attr-producto');

			$.ajax({
	            url: "{{url('admin/multimedia')}}/"+id,
				type: 'DELETE',
	            dataType: "JSON",
	            data: {
	                "id": id,
	                "_method": 'DELETE',
	                "_token": $('meta[name="csrf-token"]').attr('content'),
	            },success: function (data) {
	                $('#imagenes_producto').load("{{url('admin/multimedia')}}/" + producto);
	                $('#deleteModal_'+id).modal('toggle');
	            },
	            error: function (data) {
	              	console.log(data)  
	            }
	        });
		});

		$('.btn_mover_imagen_izquierda').click(function()
		{
			var id 			= $(this).attr('attr-id');
			var producto 	= $(this).attr('attr-producto');
			
			$('.cargando').show();

			$.ajax({
	            url: "{{url('admin/multimedia/mover/izquierda')}}/"+id,
				type: 'GET',
	            data: {
	                "_token": $('meta[name="csrf-token"]').attr('content'),
	            },success: function (data) 
	            {
	            	$('#imagenes_producto').load("{{url('admin/multimedia')}}/" + producto);
	            }
	        }).done(function()
	        {
	        	$('.cargando').fadeOut();
	        });
		});

		$('.btn_mover_imagen_derecha').click(function()
		{
			var id 			= $(this).attr('attr-id');
			var producto 	= $(this).attr('attr-producto');

			$('.cargando').show();

			$.ajax({
	            url: "{{url('admin/multimedia/mover/derecha')}}/"+id,
				type: 'GET',
	            data: {
	                "_token": $('meta[name="csrf-token"]').attr('content'),
	            },success: function (data) {
	                $('#imagenes_producto').load("{{url('admin/multimedia')}}/" + producto);
	            }
	        }).done(function()
	        {
	        	$('.cargando').fadeOut();
	        });
		});
	});
</script>
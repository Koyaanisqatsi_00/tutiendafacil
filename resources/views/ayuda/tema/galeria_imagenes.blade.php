
<link rel="stylesheet" href="{{asset('css/baguetteBox.css')}}">
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">

<div class="tz-gallery">
	<div class="row">
	    
        @foreach($producto->multimedia->sortBy('numero') as $imagen)
			@if($loop->first)
				<div class="col-12">
		            <a class="lightbox" href="{{asset($imagen->url)}}">
		            	<img src="{{asset($imagen->url)}}" alt="" width="100%">
		            </a>
				</div>
			@else
				<div class="col-4">
	                <a class="lightbox" href="{{asset($imagen->url)}}">
	                	<img src="{{asset($imagen->url)}}" alt="" width="100%">
	                </a>
				</div>
			@endif
			
		@endforeach
	</div>	
</div>

<script src="{{asset('js/baguetteBox.js')}}"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
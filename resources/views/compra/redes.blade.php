@extends('layouts.compra')

@section('contenido')
	<!-- Vista creada para ver todas las opciones de pago de mercadopago -->
  	@include('ayuda.cargando')
  	<div class="row">
  		<div class="col-12">
  			<div class="alert alert-danger" role="alert" style="display:none;">
			</div>
  			<h4><b>Pago en redes de cobranza</b></h4>
  		</div>
  		<div class="col-12">
  			<hr>
  		</div>
  		<div class="col-12">
			<form id="form-checkout" action="{{url('checkout/pago/redes')}}" method="post" class="form-horizontal">
				@csrf
				<div class="row">
					<div class="form-group col-12 col-md-6">
						<label for="name">Nombre</label>
						<input id="form-checkout__payerFirstName" name="name" type="text" class="form-control" placeholder="Nombre">
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="lastname">Apellido</label>
						<input id="form-checkout__payerLastName" name="lastname" type="text" class="form-control" placeholder="Apellido">
					</div>
					<div class="form-group col-12">
						<label for="email">E-mail</label>
						<input id="form-checkout__email" name="email" type="text" class="form-control" placeholder="Email">
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="identificationType">Tipo de documento</label>
						<select id="form-checkout__identificationType" name="identificationType" type="text" class="form-control">
							<option value="CI">CI</option>
							<option value="Otro">Otro</option>
						</select>
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="identificationNumber">Número del documento</label>
						<input id="form-checkout__identificationNumber" name="identificationNumber" type="text" class="form-control" placeholder="Numero de documento">
					</div>
					<div class="form-group col-12">
						<label for="identificationType">Red de cobranza</label>
						<select id="form-checkout__identificationType" name="payment_method_id" type="text" class="form-control">
							<option value="abitab">Abitab</option>
							<option value="redpagos">Redpagos</option>
						</select>
					</div>
					<div class="col-12">
						<hr>
					</div>

					<div class="col-6">
						<a href="{{URL::Previous()}}" class="btn btn-secondary btn-block">
							<i class="fa fa-chevron-left"></i>
							Atras
						</a>
					</div>
					<div class="col-6">
						<input type="hidden" name="transactionAmount" id="transactionAmount" value="{{(int)$venta->precio - (int)$venta->descuento}}">
						<input type="hidden" name="description" id="description" value="{{$venta->codigo}}">
						<button type="submit" class="btn btn-info btn-block">
							<i class="fa fa-paper-plane"></i>
							Enviar
						</button>
					</div>
				</div>
			</form>
  		</div>
    </div>
@endsection

@section('js')
	<script src="https://sdk.mercadopago.com/js/v2"></script>
	<script>
		const mp = new MercadoPago('{{$venta->empresa->configuracion->mp_public_key}}');
	</script>
@endsection
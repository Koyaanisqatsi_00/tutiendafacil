@extends('layouts.compra')

@section('contenido')
	<!-- Vista creada para ver todas las opciones de pago de mercadopago -->
  	@include('ayuda.cargando')
  	<div class="row">
  		<div class="col-12">
  			<div class="alert alert-danger" role="alert" style="display:none;">

			</div>
  			<h4><b>Realizar el pago</b></h4>
  		</div>
  		<div class="col-12">
  			<hr>
  		</div>
  		<div class="col-12">
  			<form id="form-checkout" class="form-horizontal">
                <progress value="0" class="progress-bar" style="display:none;">Cargando...</progress>
				<div class="row">
					<div class="form-group col-12">
						<label for="">Titular de la tarjeta</label>
						<input class="form-control name_card" type="text" name="cardholderName" id="form-checkout__cardholderName" required autofocus/>
					</div>
					<div class="form-group col-12">
						<label for="">Numero de tarjeta</label>
						<input class="form-control number_card" type="text" name="cardNumber" id="form-checkout__cardNumber" maxlength="16" required />
					</div>
					<div class="form-group col-6">
						<label for="">Fecha de expiración</label>
						<input class="form-control expiration_card" type="text" name="cardExpirationDate" id="form-checkout__cardExpirationDate" maxlength="5" required />
					</div>
					<div class="form-group col-6">
						<label for="">CCV</label>
						<input class="form-control" type="text" name="securityCode" id="form-checkout__securityCode" maxlength="3" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required />
					</div>
                    <div class="form-group col-6 hide">
                        <label for="">Banco emisor</label>
                        <select class="form-control issuer" name="issuer" id="form-checkout__issuer" required ></select>
                    </div>
                    <div class="form-group col-6 hide">
                        <label for="">Cuotas</label>
                        <select class="form-control" name="installments" id="form-checkout__installments" required ></select>
                    </div>  
					<div class="form-group col-12">
						<label for="">Email</label>
						<input class="form-control" type="email" name="cardholderEmail" id="form-checkout__cardholderEmail" required />
					</div>
					<div class="form-group col-6">
						<label for="">Tipo de documento</label>
						<select class="form-control" name="identificationType" id="form-checkout__identificationType" required ></select>
					</div>
					<div class="form-group col-6">
						<label for="">Documento</label>
						<input class="form-control" type="text" name="identificationNumber" id="form-checkout__identificationNumber" required />
					</div>
					<div class="col-12">
						<hr>
					</div>
					<div class="form-group col-6">
						<a href="{{URL::Previous()}}" class="btn btn-secondary btn-block px-5">
							<i class="fa fa-chevron-left"></i>
							Atras
						</a>
					</div>	
					<div class="form-group col-6 text-right">
						<button type="submit" id="form-checkout__submit" class="btn btn-info btn-block px-5">
	                   		<i class="fa fa-money"></i>
	                   		Pagar
	                   	</button>
					</div>
				</div>
            </form>
  		</div>
  		<div class="col-12">
  			<hr>
  		</div>
  		<div class="col-12 text-center">
  			<p>O</p>
  			<p><a href="{{url('checkout/pago/redes', $venta->codigo)}}">Pagar en redes de cobranza</a></p>
  		</div>
    </div>
@endsection

@section('js')
	<script src="https://www.mercadopago.com/v2/security.js" view="card"></script>
	<script src="https://sdk.mercadopago.com/js/v2"></script>
    <script>
    	$('.name_card').keyup(function()
    	{	
    		if($(this).val() === "")
    			$('.name_card_text').html("Nombre Apellido");
    		else
    			$('.name_card_text').html($(this).val());


    		$('.issuer_name').html($('.issuer').text().toUpperCase());
    	});

	  	$('.number_card').bind('keyup paste', function(){
		    this.value = this.value.replace(/[^0-9]/g, '');
		});

    	$('.number_card').keyup(function()
    	{
			if($(this).val().length < 13)
			{
				$('.number_card_text').html('····');
			}else if($(this).val().length == 13)
    		{
    			$('.number_card_text').html($(this).val().substr(-1) + '···');
    		}else if($(this).val().length == 14)
    		{
    			$('.number_card_text').html($(this).val().substr(-2) + '··');
    		}else if($(this).val().length == 15)
    		{
    			$('.number_card_text').html($(this).val().substr(-3) + '·');
    		}else if($(this).val().length == 16)
    		{
    			$('.number_card_text').html($(this).val().substr(-4) );
    		}

    		$('.issuer_name').html($('.issuer').text().toUpperCase());
    	});

    	$('.expiration_card').keyup(function()
    	{
    		if($(this).val().length == 0)
    		{
    			$('.expiration_card_text').html('MM/YY');
    		}else if($(this).val().length == 1)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-1) + 'M/YY');
    		}else if($(this).val().length == 2)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-2) + '/YY');
    		}else if($(this).val().length == 4)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-4) + 'Y');
    		}else if($(this).val().length == 5)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-5));
    		}
    		
    		$('.issuer_name').html($('.issuer').text().toUpperCase());
    	});

       	const mp = new MercadoPago('{{$venta->empresa->configuracion->mp_public_key}}');
	    const cardForm = mp.cardForm({
	      amount: "{{(int)$venta->precio - (int)$venta->descuento}}",
	      autoMount: true,
	      form: {
	        id: "form-checkout",
	        cardholderName: {
	          id: "form-checkout__cardholderName",
	          placeholder: "Titular de la tarjeta",
	        },
	        cardholderEmail: {
	          id: "form-checkout__cardholderEmail",
	          placeholder: "E-mail",
	        },
	        cardNumber: {
	          id: "form-checkout__cardNumber",
	          placeholder: "Número de la tarjeta",
	        },
	        cardExpirationDate: {
	          id: "form-checkout__cardExpirationDate",
	          placeholder: "Fecha de vencimiento (MM/YY)",
	        },
	        securityCode: {
	          id: "form-checkout__securityCode",
	          placeholder: "Código de seguridad",
	        },
	        installments: {
	          id: "form-checkout__installments",
	          placeholder: "Cuotas",
	        },
	        identificationType: {
	          id: "form-checkout__identificationType",
	          placeholder: "Tipo de documento",
	        },
	        identificationNumber: {
	          id: "form-checkout__identificationNumber",
	          placeholder: "Número de documento",
	        },
	        issuer: {
	          id: "form-checkout__issuer",
	          placeholder: "Banco emisor",
	        },
	      },
	      callbacks: {
	        onFormMounted: error => {
	          if (error) return console.warn("Form Mounted handling error: ", error);
	        },
	        onSubmit: event => {
	        	$('.cargando').fadeIn();
	          	event.preventDefault();

	          	const {
	                cardholderName,
		            paymentMethodId: payment_method_id,
		            issuerId: issuer_id,
		            cardholderEmail: email,
		            amount,
		            token,
		            installments,
		            identificationNumber,
		            identificationType,
	          	} = cardForm.getCardFormData();

	          	$.ajax({
	          		type:'POST',
					url:"{{url('checkout/pago/tarjeta')}}",
					headers: {
		              "Content-Type": "application/json",
		            },
					data:JSON.stringify({
		                _token:"{{csrf_token()}}",
		              	token,
		              	issuer_id,
		              	payment_method_id,
	                    cardholderName,
		              	transaction_amount: Number(amount),
		              	installments: Number(installments),
		              	description: "{{$venta->codigo}}",
		              	payer: {
		                	email,
		                	identification: {
		                  		type: identificationType,
		                  		number: identificationNumber,
		                	},
		              	},
	          		}),
          		 	success: function (result) 
          		 	{
          		 		if(result.status == "approved")
                        {
                            window.location.replace("{{url('eticket', $venta->codigo)}}");
                        }
                        else
                        {
                        	$('.alert').html(result.status_detail);
                        	$('.alert').fadeIn();

                        	$('input').val("");
                        	$("select option:selected").prop("selected", false);
                        }

          		 		$('.cargando').fadeOut();
			        },
	          	});
	        },
	        onFetching: (resource) => {
	       		
	        }
	      },
	    });
    </script>
@endsection